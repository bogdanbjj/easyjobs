# -*- coding: utf-8 -*-
import codecs
import unicodecsv as csv
import json
import xlwt

from easyjobs.items import ItalianDoctorsItem, AagDirectoryItem


class JsonPipeline(object):
    def __init__(self):
        self.filename = "WebSummit_output.json"
        self.file = codecs.open(self.filename, "w", encoding='utf-8')

    def process_item(self, item, spider):
        line = json.dumps(dict(item), ensure_ascii=False) + ',' + "\n"
        self.file.write(line)
        return item

    def get_name(self, spider):
        return spider.__class__.__name__

    def spider_close(self, spider):
        self.file.close()


class ExcelPipeline(object):
    def __init__(self):
        self.col = 1
        self.wb = xlwt.Workbook()
        self.sh = None

    def process_item(self, item, spider):
        if isinstance(item, AagDirectoryItem):
            self.name = 'AagdDirectory'
            if not self.sh:
                self.sh = self.add_sheet(name=self.name, item=item)

        elif isinstance(item, ItalianDoctorsItem):
            self.name = 'IDoctors'
            if not self.sh:
                self.sh = self.add_sheet(name=self.name, item=item)

        keys = sorted(item.keys())
        for i in range(len(keys)):
            self.sh.write(self.col, i, item.get(keys[i]))

        self.col += 1
        self.wb.save(spider.__class__.__name__+'.xls')

    def add_sheet(self,  name, item):
        keys = sorted(item.keys())
        sh = self.wb.add_sheet(sheetname=name)
        for i in range(len(keys)):
            sh.write(0, i, keys[i])

        return sh


class CSVPipeline(object):
    file_exists = False

    def process_item(self, item ,spider):
        with open(spider.__class__.__name__, 'ab') as f:
            w = csv.DictWriter(f, item.keys(), delimiter=' ')
            if not self.file_exists:
                w.writeheader()
                self.file_exists = True
            w.writerow(item)