from scrapy.http import Request
from scrapy.spiders import Spider

from easyjobs.items import ShopifyItem


class ShopifyExpertsProfileSpider(Spider):
    name = 'se'
    allowed_domains = ['shopify.com']
    start_urls = [
        'https://experts.shopify.com/{}?page={}'.format(i, j)
        for i in ('developers', 'setup-experts', 'designers')
        for j in range(1, 24)]
    base_url = 'https://experts.shopify.com'

    def parse(self, response):
        # developer ii pana la pagina 14
        # designers merge pana la pagina 18
        # si setup-exp pana la 24

        if 'Sorry' in response.body:
            return
        print
        print 'url ', response.url
        print
        # get all profile links
        # and send them to parse_profiles
        profiles = response.xpath("//div[@class='detail-wrapper clearfix']/h3/a/@href").extract()
        for profile in profiles:
            url = self.base_url + profile
            yield Request(url=url, callback=self.parse_profiles)

    def parse_profiles(self, response):
        si = ShopifyItem()
        company_name = response.xpath("//span[@itemprop='name']/text()").extract()
        location = response.xpath("//a[@class='expert-info-icon-link']/text()").extract()
        starting_from = response.xpath("//span[@class='faded']/text()").extract()
        st = starting_from[0].strip()
        website = response.xpath("//a[@_target='blank']/@href").extract()
        si['comp_name'] = company_name
        si['location'] = location
        si['starting_from'] = st
        si['website'] = website
        yield si

