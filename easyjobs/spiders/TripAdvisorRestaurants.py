from scrapy.http import Request
from scrapy.spiders import Spider


from easyjobs.items import TripAdvisorItem


class TripAdvisorRestaurantSpider(Spider):
    name = 'tar'
    download_delay = 3
    allowed_domains = ['tripadvisor.com']
    start_urls = ['https://www.tripadvisor.com/Restaurants-g293916-Bangkok.html',
                  # 'https://www.tripadvisor.com/Restaurants-g1722390-Cape_Town_Western_Cape.html',
                  # 'https://www.tripadvisor.com/Restaurants-g304551-New_Delhi_National_Capital_Territory_of_Delhi.html',
                  # 'https://www.tripadvisor.com/Restaurants-g255060-Sydney_New_South_Wales.html',
                  # 'https://www.tripadvisor.com/Restaurants-g255100-Melbourne_Victoria.html',
                  ]

    def parse(self, response):
        """
        gets all the restaurants and
        then opens next page
        """
        base_url = response.url.split("R")[0]
        import ipdb;ipdb.set_trace()
        restaurants = response.xpath("//a[@class='property_title']/@href").extract()
        for rest in restaurants:
            url = base_url + rest
            yield Request(url=url, callback=self.parse_restaurant)

        # click on the next page
        n_page = response.xpath("//div[@class='unified pagination js_pageLinks']/a/@href").extract()
        if n_page:
            if len(n_page) == 2:
                n_page = n_page[1]
            else:
                n_page = n_page[0]
            url = base_url + n_page
            yield Request(url=url, callback=self.parse)

    def parse_restaurant(self, response):
        ta = TripAdvisorItem()
        name = response.xpath("//h1[@property='name']/text()").extract()[1].strip()
        address = response.xpath("//span[@property='address']/span/span/text()").extract()
        city = address.pop(-1)
        add = [a.strip() for a in address]
        address = " ".join(add)
        phone = response.xpath("//div[@class='fl phoneNumber']/text()").extract()
        ta['restaurant_name'] = name
        ta['address'] = address
        ta['city'] = city
        ta['phone'] = phone
        ta['source'] = response.url
        yield ta




