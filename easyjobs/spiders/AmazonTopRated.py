from scrapy.http import Request
from scrapy.spiders import Spider

import xlwt


class AmazonTopRatedSpider(Spider):
    name = 'amazon'
    allowed_domains = ['amazon.com']
    start_urls = ['https://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Dbaby-products&field-keywords=Top+Rated']
    items = []
    wb = xlwt.Workbook()
    sheet = wb.add_sheet('Top Rated Baby Products')

    def parse(self, response):
        """Gets the 24 links and makes request
        next page. When list hits 100 links close spider"""
        if len(self.items) > 100:
            self.write_to_xls(self.items)
            return

        div = response.xpath("//div[@class='a-row a-spacing-top-mini']")

        products = div.xpath(
            "./a[@class='a-link-normal s-access-detail-page  a-text-normal']/@href").extract()
        self.items.extend(products)

        urm_pag = response.xpath("//a[@class='pagnNext']/@href").extract()
        url = urm_pag[0]
        link = response.url.split("/")
        url = link[0] + '//' + link[2] + url

        yield Request(url=url, callback=self.parse)

    def write_to_xls(self, items):
        if len(items) > 100:
            items.insert(0, None)
            print items[0]
            items = items[1:101]
            self.sheet.write(0, 0, "Top Rated Baby Products")
            for i in range(len(items)):
                self.sheet.write(i+1, 0, items[i])
        AmazonTopRatedSpider.wb.save("Baby Products.xls")

    def read_from_xls(self):
        """Get the """
        import xlrd
        wb = xlrd.open_workbook('name_of_xls.xls')
        print wb.sheet_names()
        sheet = wb.sheet_by_index() # or wb.sheet_by_

        # name()
        # once i have the sheet iterate over the rows













