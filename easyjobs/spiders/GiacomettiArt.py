from scrapy.http import Request
from scrapy.spiders import Spider


class GiacomettiArtSpider(Spider):
    name = 'ga'
    allowed_domains = ['fondation-giacometti.fr/']
    start_urls = [
        'http://www.fondation-giacometti.fr/en/content/art/16/ajax_1/18/ajax_2/'
        '19/ajax_3/?start=0&limit=1479&agd=&faag=&title=&qstr=&foundation%5B%5D=1&database%5B%5D=1']
    base_url = 'http://www.fondation-giacometti.fr'

    def parse(self, response):
        """Makes requests for individual pages"""
        links = response.xpath("//a[@class='work-title dispNone']/@href").extract()
        for link in links:
            l = link.split('/')[1:-1]
            link = '/' + l[0] + '/content/' + l[1] + '/' + l[2] + '/' + l[1] + '-' + l[2] +'/'
            url = self.base_url + link
            yield Request(url=url, callback=self.parse_art, dont_filter=True)

    def parse_art(self, response):
        """IM in!!!!
            Process every individual page
        """
        images = response.xpath(
            "//figure[@class='BG2 tableCell fullH fullW txtM margin0']/img/@src").extract()
        if images:
            images = [(self.base_url + img) for img in images]
        title = response.xpath("//h3[@class='link fs135 bold']/text()").extract()
        if title:
            title = title[0].strip()
        agd = response.xpath("//h2[@class='link fs135']/text()").extract()
        if agd:
            agd = agd[0].strip()
        creation_date = response.xpath("//li[@class='lined VpaddingSm']/text()")[0].extract()
        if creation_date:
            creation_date = creation_date.strip()
        technique = response.xpath("//li[@class='lined VpaddingSm']/text()")[1].extract()
        if technique:
            technique = technique.strip()
        dimensions = response.xpath("//li[@class='lined VpaddingSm']/text()")[2].extract()
        if dimensions:
            dimensions = dimensions.strip()

        # provenance info is hard to get because there are multiple tags with same class names
        # so untill i find a better solution, will get ALL info
        # (inscription, description, exhibition, bibliography)
        provenance = response.xpath("//div[@class='txt margin paddingSm BpaddingLg']/text()").extract()
        provenance = ''.join(provenance)

        # TO DO: still need to create an item and write to some pipeline
        #    excel or csv
         
        yield # item



