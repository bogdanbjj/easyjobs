from scrapy.http import Request, FormRequest
from scrapy.shell import inspect_response
from scrapy.spiders import Spider


from selenium import webdriver


class NewMemorialsDirectItemSpider(Spider):
    name = 'nmd'
    allowed_domains = ['newmemorialsdirect.com']
    start_urls = ['https://www.newmemorialsdirect.com/login.aspx']
    pages = [
        'http://www.newmemorialsdirect.com/wholesale-cremation-jewelry.aspx?page={}&size=200'.format(
            i) for i in range(1, 4)]

    def start_requests(self):
        return self.init_request()

    def init_request(self):
        return [Request(url=self.start_urls[0], callback=self.login)]

    def login(self, response):
        view_state = response.xpath("//input[@name='__VIEWSTATE']/@value")[0].extract()

        formdata = {'ctl00$pageContent$loginRegister$txtEmail': 'james@afterlifeessentials.com',
                   'ctl00$pageContent$loginRegister$txtPassword': 'nmdessentials',
                    '__VIEWSTATE': view_state}

        return [
            FormRequest.from_response(response, formdata=formdata, callback=self.parse)
            ]

    def parse(self, response):
        inspect_response(response, self)
        view_state = response.xpath("//input[@name='__VIEWSTATE']/@value")[0].extract()
        for page in self.pages:
            yield FormRequest(
                url=page, formdata={'__VIEWSTATE': view_state}, callback=self.parse_pages)

    def parse_pages(self, response):
        inspect_response(response, self)
        items = response.xpath("//div[@product-list-control]/@href").extract()
        # print len(items)
