from scrapy.http import Request
from scrapy.shell import inspect_response
from scrapy.spiders import Spider

from easyjobs.items import AagDirectoryItem


class AagDirectorySpider(Spider):
    name = 'aag'
    allowed_domains = ['']
    owner_url = 'http://www.aagddirectory.com/?sid=4:owners&site={}&itemid=468'
    apartment_url = 'http://www.aagddirectory.com/?sid=3:apartments&site={}&itemid=468'
    base_url = 'http://www.aagddirectory.com'
    start_urls = [apartment_url.format(i) for i in range(1, 124)] #124
    start_urls.extend([owner_url.format(i) for i in range(1, 67)]) # 67

    def parse(self, response):
        a = AagDirectoryItem()
        links = response.xpath("//span[@class='lead']/a/@href").extract()

        if 'owners' in response.url:
            a['type'] = 'Owner'
        elif 'apartments' in response.url:
            a['type'] = 'Apartment'

        for link in links:
            url = self.base_url + link
            yield Request(url=url, meta={'A': a}, callback=self.parse_details, dont_filter=True)

    def parse_details(self, response):
        a = response.meta['A']
        main = response.xpath("//div[@class='SPDetailEntry']")
        first_name = main.xpath("./div[@class='spField field_first_name']/text()").extract()
        last_name = main.xpath("./div[@class='spField field_last_name']/text()").extract()
        name = first_name[0] + " " + last_name[0] if first_name and last_name else ''
        a['name'] = name
        company = main.xpath("./h1/text()").extract()
        a['company'] = company[0] if company else ''
        address = main.xpath("./div[@class='spField field_address1']/text()").extract()
        a['address'] = address[0] if address else ''
        state = main.xpath("./div[@class='spField field_state']/text()").extract()
        a['state'] = state[0] if state else ''
        zip_code = main.xpath("./div[@class='spFieldsData field_zip']/text()").extract()
        a['zip'] = zip_code[0] if zip_code else ''
        city = main.xpath("./div[@class='spFieldsData field_city']/text()").extract()
        a['city'] = city[0] if city else ''
        phone = main.xpath("./div[@class='spFieldsData field_phone']/text()").extract()
        a['phone'] = phone[0] if phone else ''
        email = main.xpath("./div[@class='spFieldsData field_email']/text()").extract()
        a['email'] = email[0] if email else ''
        yield a
