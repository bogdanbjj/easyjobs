from scrapy.http import Request
from scrapy.spiders import Spider

from urlparse import urljoin

from easyjobs.items import IcResourceItem


class IcResourcesJobSpider(Spider):
    name = 'ic'
    start_urls = [
                    'http://www.ic-resources.com/page/{}/?s'.format(i)
                    for i in range(1, 61)
                  ]
    allowed_domains = ['ic-resources.com']

    def parse(self, response):
        """get all links and pass them forward
        to parse job"""
        jobs = response.xpath("//li[@class='job']/a/@href").extract()
        for job in jobs:
            url = urljoin(response.url, job)
            yield Request(url=url, callback=self.parse_job)

    def parse_job(self, response):
        """gets all the info and yields item"""
        ic = IcResourceItem()
        job_title = response.xpath("//div[@class='single-job--main']/h1/text()").extract()
        if job_title:
            job_title = job_title[0]
        job_descr = response.xpath("//div[@class='single-job--main']/p/text()").extract()
        if job_descr:
            job_descr = [i.strip() for i in job_descr]
            job_descr = " ".join(job_descr).strip()
        job_details = response.xpath("//div[@class='single-job--side']/ul/li/text()").extract()
        job_details = [i.strip() for i in job_details]
        jd = " ".join(job_details)
        link = response.url
        ic['job_title'] = job_title
        ic['job_descr'] = job_descr
        ic['job_details'] = jd
        ic['link'] = link
        yield ic

