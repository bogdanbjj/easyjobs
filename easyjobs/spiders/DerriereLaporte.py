from scrapy.http import Request
from scrapy.spiders import Spider


from easyjobs.items import DerriereLaporteItem


class DerrierLaporteProductSpider(Spider):
    name = 'dl'
    allowed_domain = ['derrierelaporte.com']
    start_urls = [
        'http://www.derrierelaporte.com/fr/1378-nos-produits#/page-{}'.format(
                                                                        i) for i in range(1, 70)]
    base_url = 'http://www.derrierelaporte.com/'

    def parse(self, response):
        items = response.xpath("//a[@class='product_img_link']/@href").extract()
        for item in items:
            yield Request(url=item, callback=self.parse_item, dont_filter=True)

    def parse_item(self, response):
        d = DerriereLaporteItem()
        # print response.url, response.status
        pic_url = response.xpath("//img[@id='bigpic']/@src").extract()
        item_name = response.xpath("//h1[@itemprop='name']/text()").extract()
        prod_ref = response.xpath("//p[@id='product_reference']/span/text()").extract()
        descr = response.xpath("//div[@id='short_description']/text()").extract()
        section_names = response.xpath("//section[@class='fiche-technique']/ul/li/span/text()").extract()
        section_values = response.xpath("//section[@class='fiche-technique']/ul/li/text()").extract()
        section_values = [i for i in section_values if i != ' ']

        fiche_tech = [u'{}{}'.format(section_names[i], section_values[i]) for i in range(len(section_names))]

        d['Picture'] = pic_url[0] if pic_url else ''
        d[u'Name'] = item_name[0] if item_name else ''
        d[u'Product_reference'] = prod_ref[0] if prod_ref else ''
        d[u'Description'] = descr[0] if descr else ''
        d[u'Fiche_technique'] = ' '.join(fiche_tech)
        yield d

