from scrapy.http import Request
from scrapy.spiders import Spider

from easyjobs.items import ItalianDoctorsItem


class IDoctorsDoctorSpider(Spider):
    name = 'id'
    allowed_domains = ['idoctors.it']
    start_urls = ['https://www.idoctors.it/']

    def parse(self, response):
        specializations = response.xpath(
        "//div[@id='col-spec-1' or @id='col-spec-2' or @id='col-spec-3' or @id='col-spec-4']/ul/li/a/@href"
                                                                                                    ).extract()
        for specialization in specializations:
            yield Request(url=specialization, callback=self.parse_locations)

    def parse_locations(self, response):

        locations = response.xpath(
            "//div[@class='colonna-specializzazioni col-xs-12 col-sm-6 col-md-3']/"
                                    "ul[@ class ='list-unstyled'] / li / a / @ href").extract()

        for location in locations:
            yield Request(url=location, callback=self.parse_doctors)

    def parse_doctors(self, response):
        doctors = response.xpath("//p[@class='box-title']/a/@href").extract()
        for doctor in doctors:
            yield Request(url=doctor, callback=self.parse_doctor)

    def parse_doctor(self, response):
        i = ItalianDoctorsItem()
        prefix, name = response.xpath("//span[@class='nome-medico']/text()")[0].extract().split('.', 1)
        specialization = response.xpath("//h2[@class='specializzazione-medico']/text()")[0].extract()
        photo = response.xpath("//img[@class='img-responsive foto-medico']/@src").extract()
        photo = photo[0] if photo else 'No photo'
        calendars = response.xpath("//div[@id='prest-top-acc-0']/ul/li")
        calendars = len(calendars)
        i['prefix'] = prefix
        i['name'] = name
        i['specialization'] = specialization
        i['photo'] = photo
        i['calendars'] = calendars
        yield i
