import json

from scrapy.http import Request
from scrapy.spiders import Spider


from easyjobs.items import WebSummitItem


class WebSummitAttendeesSpider(Spider):
    name = 'ws'
    allowed_domains = ['websummit.net']
    start_urls = ['https://api.cilabs.net/v1/conferences/ws16/info/attendees?limit=15000&page={}'.format(
                                                                                        i) for i in range(1,8)]
    ids = {}
    l = []

    def parse(self, response):
        data = json.loads(response.body)
        self.l.extend(data['attendees'])
        print 'Extending list ', len(self.l)

        for i, info in enumerate(self.l):
            if self.ids.get(info['id'], False):
                continue
            else:
                self.ids['id'] = info['id']
                w = WebSummitItem()
                w['name'] = info['name']
                w['avatar'] = info['avatar_url']
                w['bio'] = info['bio']
                w['career'] = info['career']
                w['company'] = info['company']
                w['country'] = info['country']
                w['id'] = info['id']
                yield w
            self.l.pop(i)
